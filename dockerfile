FROM maven:3.6.1-jdk-8 AS build


COPY src /home/app/src
COPY pom.xml /home/app

RUN mvn -f /home/app/pom.xml clean package

FROM openjdk:8u111-jdk-alpine
COPY --from=build /home/app/target/system-service.jar /usr/local/lib/system-service.jar
EXPOSE 8082
ENTRYPOINT ["java","-jar","/usr/local/lib/system-service.jar"]