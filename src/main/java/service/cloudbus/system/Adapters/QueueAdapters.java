package service.cloudbus.system.Adapters;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import org.springframework.web.client.RestTemplate;

@Service

public class QueueAdapters {
    
    @Value("${cloudbus.queueservice.url}")
    private String queueServiceUrl;

    public Long getCountQueue(long subjectId) {

        RestTemplate restTemplate = new RestTemplate();
        
        return restTemplate.getForEntity(this.queueServiceUrl, Long.class).getBody();

    }



}