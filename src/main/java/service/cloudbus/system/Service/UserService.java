package service.cloudbus.system.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.cloudbus.system.Exceptions.NotFoundException;
import service.cloudbus.system.Model.Passenger;
import service.cloudbus.system.Model.User;
import service.cloudbus.system.Repository.UserRepository;

import java.util.Date;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private PassengerService passengerService;

    public Passenger loginUser(User user) {
        User targetUser = null;
        try {
            targetUser = userRepo.findByUsernameAndPassword(user.getUsername(), user.getPassword());
        } catch (Exception e) {
            System.out.println(e);
        }
        if (targetUser != null) {
            return passengerService.getPassenger(targetUser);
        } else {
            throw new NotFoundException("user id:" + user.getId() + "not found");
        }
    }

    public User findById(long targetPassenger) {
        return userRepo.findById(targetPassenger).get();
    }

    public Passenger facebookLogin(Passenger passenger) {
        User anyUserWithLoginId = userRepo.findByFacebookId(passenger.getUser().getFacebookId());
        if (anyUserWithLoginId == null) {
            return passengerService.register(passenger);
        } else {
            return passengerService.getPassenger(anyUserWithLoginId);
        }
    }

    public User banUser(User targetUser) {
        targetUser.setBanned(new Date());
        return userRepo.save(targetUser);
    }
}
