package service.cloudbus.system.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import service.cloudbus.system.Model.BusDriver;
import service.cloudbus.system.Repository.BusDriverRepository;

@Service
public class BusDriverService{
    
    @Autowired
    private BusDriverRepository busDriverRepository;

    public BusDriver createBusDriver(BusDriver busDriver){
        BusDriver busDriver1 = busDriverRepository.save(busDriver);
        return busDriver1;
    }

    public BusDriver getBusDriverById(long driverId){
        return busDriverRepository.getOne(driverId);
    }

    public List<BusDriver> getAllDriver(){
        return busDriverRepository.findAll();
    }

    public BusDriver deleteDriver(long driverId){
        BusDriver targetDriver = busDriverRepository.getOne(driverId);
        busDriverRepository.delete(targetDriver);
        return targetDriver;
    }
    
}