package service.cloudbus.system.Service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import service.cloudbus.system.Model.Route;
import service.cloudbus.system.Model.RouteList;
import service.cloudbus.system.Repository.RouteRepository;

@Service
public class RouteService {

    @Autowired
    private RouteRepository routeRepo;

    public List<Route> getAllRoute(){
        return routeRepo.findAll();
    }

    public List<Route> getRouteByDeparture(RouteList targetDeparture) {
        return routeRepo.findByDeparture(targetDeparture);
    }

    public Route saveRoute(Route inputRoute){
        return routeRepo.save(inputRoute);
    }

    public Route getRouteById(long tragetId){
        return routeRepo.getOne(tragetId);
    }

}