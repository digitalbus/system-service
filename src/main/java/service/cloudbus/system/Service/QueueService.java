package service.cloudbus.system.Service;

import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import service.cloudbus.system.Exceptions.LockedException;
import service.cloudbus.system.Exceptions.NotFoundException;
import service.cloudbus.system.Exceptions.QueuingException;
import service.cloudbus.system.Exceptions.ConflictException;
import service.cloudbus.system.Model.*;
import service.cloudbus.system.Model.Queue;
import service.cloudbus.system.Repository.QueueRepository;
import service.cloudbus.system.Producer.MessageSender;
import service.cloudbus.system.Repository.TimetableRepository;
import service.cloudbus.system.Utils.PojoToJson;

import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

import static service.cloudbus.system.Utils.Timeutils.convertWeekDayToTimetableDay;

@Service
public class QueueService {
    @Autowired
    private QueueRepository queueRepository;

    @Autowired
    private TimetableService timetableService;

    @Autowired
    private UserService userService;

    @Autowired
    private MessageSender sender;

    @Autowired
    private PassengerService passengerService;

    @Autowired
    private RestHighLevelClient client;

    @Autowired
    private RouteService routeService;

    private static int emptySeat = 0;

    @Autowired
    private TimetableRepository timetableRepo;

    @Autowired
    private TripService tripService;

    @Autowired
    private BusService busService;

    @Autowired
    private MessageSender messageSender;

    @Value("${cloudbus.useElasticsearch}")
    private boolean useElasticsearch;


    public Queue bookQueue(Queue queue) {
        long targetPassenger = queue.getPassenger().getId();
        Queue anyQueueFromSamePassenger = queueRepository.findByPassengerIdAndTimetableIdAndQueueType(
                targetPassenger,
                queue.getTimetable().getId(),
                QueueType.booked
        );
        try {
            passengerService.findById(targetPassenger);

        } catch (Exception e) {
            throw new NotFoundException("Bad UserId Request");
        }
        long timetableCurrentQueue = countActiveQueueByTimetable(queue.getTimetable().getId());
        Timetable targetTimetable = timetableService.getTimetableById(queue.getTimetable().getId());
        int maximumPassenger;
        maximumPassenger = getMaximumPassenger(targetTimetable.getBusType());
        if (anyQueueFromSamePassenger != null) {
            throw new ConflictException("This passenger already have queue");
        }
        if (timetableCurrentQueue < maximumPassenger) {
            sendFetchMessage(targetTimetable.getRoute().getId());
            Queue savedQueue = queueRepository.save(queue);
            if(useElasticsearch == true)sendToElasticSearch(savedQueue);
            return savedQueue;
        } else {
            throw new LockedException("Queue is Full");
        }
    }

    public Queue createQueue(Queue queue) {
        return queue;
    }

    private String sendToElasticSearch(Queue queue) {
//        Queue savedQueue = queueRepository.save(queue);
        Queue targetQueue = queueRepository.getOne(queue.getId());
        queue.setPassenger(passengerService.findById(queue.getPassenger().getId()));
        Timetable timetable = timetableService.getTimetableById(queue.getTimetable().getId());
        Route route = routeService.getRouteById(timetable.getRoute().getId());
        Map<String, Object> json = PojoToJson.queueToKibanaJson(targetQueue, targetQueue.getPassenger(), timetable, route);
        IndexRequest indexRequest = new IndexRequest("queue", "_doc", queue.getId() + "");
        indexRequest.source(json);
        IndexResponse indexResponse = null;
        try {
            indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
            String elasticEventid = indexResponse.getId();
            return elasticEventid;
        } catch (IOException e) {
            e.printStackTrace();
            return "elastic fail";
        }
    }

    private void sendFetchMessage(long targetRouteId) {
        sender.sendFetchMessage(targetRouteId, true);
    }

    public int getMaximumPassenger(BusType busType) {
        int maximumPassenger;
        if (busType == BusType.bus) {
            maximumPassenger = 30;
        } else {
            maximumPassenger = 15;
        }
        return maximumPassenger;
    }

    public Queue getQueueById(long id) {
        try {
            return queueRepository.findById(id).get();
        } catch (Exception e) {
            throw new NotFoundException("queue not found with id:" + id);
        }
    }

    public Queue checkinQueueById(long id) {
        Queue targetQueue = queueRepository.findByIdAndQueueType(id, QueueType.booked);
        if (targetQueue == null) {
            throw new NotFoundException("No queue booked queue with id:" + id+
                    " or may be queue is in a requested or reserved state");
        }
        BusType busType = targetQueue.getTimetable().getBusType();
        long currentQueue = countActiveQueueByTimetable(targetQueue.getTimetable().getId());
        long tripQueue = 0;
        if (targetQueue.getTrip() != null) {
            targetQueue.getTrip().getQueues().size();
        }
        int maximumPassenger = getMaximumPassenger(busType);
        if (currentQueue < maximumPassenger || tripQueue < maximumPassenger) {
            messageSender.sendCheckedInMessage(targetQueue.getId());
            targetQueue.setQueueType(QueueType.checkedin);
            return queueRepository.save(targetQueue);
        } else {
            throw new QueuingException("Queue is Full");
        }

    }

    public List<Queue> getCheckinStatusByTimetableId(long timetableId) {
        List<Queue> queues = queueRepository.findByTimetableIdAndQueueTypeAndTripIsNull(timetableId, QueueType.booked);
        queues.addAll(queueRepository.findByTimetableIdAndQueueTypeAndTripIsNull(timetableId, QueueType.reserved));
        queues.addAll(queueRepository.findByTimetableIdAndQueueTypeAndTripIsNull(timetableId, QueueType.checkedin));
        return queues;
    }

    public List<Queue> getCheckinStatusByTripId(long tripId) {
        return tripService.getById(tripId).getQueues();
    }

    public long countTimetableByTimetableId(long timetableId) {
        return queueRepository.countByTimetableId(timetableId);
//        return 5l;
    }

    public long countActiveQueueByTimetable(long timetableId) {
        long activeQueue = queueRepository.countByTimetableIdAndQueueType(timetableId, QueueType.checkedin);
        activeQueue += queueRepository.countByTimetableIdAndQueueType(timetableId, QueueType.reserved);
        activeQueue += queueRepository.countByTimetableIdAndQueueType(timetableId, QueueType.booked);
        return activeQueue;
//        return 5l;
    }

    public long countRequestedQueue(long timetableId) {
        long activeRequest = queueRepository.countByTimetableIdAndQueueType(timetableId, QueueType.requested);
        return activeRequest;
    }

    public List<Queue> getMyTicket(long targetUserId) {
        List<Queue> queues = queueRepository.findByPassengerIdAndQueueTypeOrQueueTypeOrderByIdDesc(targetUserId, QueueType.booked, QueueType.requested);
        for (Queue curQueue : queues) {
            curQueue.setQueueNumber(getQueueNumberById(curQueue.getId()));
        }
        return queues;
    }

    public int getQueueNumberById(long queueId) {
        Queue targetQueue = getQueueById(queueId);
        long targetTimetableId = targetQueue.getTimetable().getId();
        List<Queue> queues = getQueueByTimetableId(targetTimetableId);
        targetQueue = compareQueue(targetQueue, queues);
        return targetQueue.getQueueNumber();
    }

    private Queue compareQueue(Queue targetQueue, List<Queue> queues) {
        for (int i = 0; i < queues.size(); i++) {
            Queue queue = queues.get(i);
            queue.setQueueNumber(i + 1);
            if (queue.getId() == targetQueue.getId()) {
                return queue;
            }
        }
        return targetQueue;
    }

    public List<Queue> getQueueByTimetableId(long targetTimetableId) {
        return queueRepository.findByTimetableId(targetTimetableId);
    }

    public List<Queue> getQueueByTimetableIdAndQueueType(long timetableId, QueueType queueType) {
        return queueRepository.findByTimetableIdAndQueueTypeAndTripIsNull(timetableId, queueType);
    }

    public List<Queue> saveAllQueue(List<Queue> queues) {
        return queueRepository.saveAll(queues);
    }

    public int sendMessage(long timetableId) {
        emptySeat++;
        sender.sendMessage(timetableId, emptySeat);
        return emptySeat;
    }

    public Queue requestQueue(Queue requestQueue) {
        return queueRepository.save(requestQueue);
    }

    public List<Timetable> getAllRequest() {
        List<Queue> requests = queueRepository.findByQueueType(QueueType.requested);
        List<Long> timetableIds = new ArrayList<>();
        List<Timetable> requestedTimetable = new ArrayList<>();
        for (Queue request : requests) {
            timetableIds.add(request.getTimetable().getId());
        }
        List<Long> distinctTimetableIds = timetableIds.stream().distinct().collect(Collectors.toList());
        for (long id : distinctTimetableIds) {
            requestedTimetable.add(timetableRepo.getOne(id));
        }
        for (Timetable timetable : requestedTimetable) {
            Timetable temp = timetableService.calculateTimetables(timetable);
            timetable.setCurrentActiveQueue(temp.getCurrentActiveQueue());
            timetable.setAvailableSeat(temp.getAvailableSeat());
            timetable.setCurrentRequestQueue(countRequestedQueue(timetable.getId()));
        }
        return requestedTimetable;
    }

    public List<Queue> approveRequets(Trip trip) {
        List<Queue> currentRequest = getQueueByTimetableIdAndQueueType(trip.getTimetable().getId(), QueueType.requested);
        Bus useageBus  = busService.getBusById(trip.getBus().getId());
        List<Queue> approve = sliceRequestQueueWithBusSeat(currentRequest, useageBus);
        for (Queue queue : approve) {
            queue.setQueueType(QueueType.booked);
            Queue reservedQueue = queue.getReservedQueue();
            queue.setReservedQueue(null);
            queueRepository.save(queue);
            deleteQueue(reservedQueue);
        }
        trip.setQueues(approve);
        tripService.save(trip);
        return approve;
    }

    private List<Queue> sliceRequestQueueWithBusSeat(List<Queue> currentRequest, Bus useageBus) {
        if(useageBus.getSeat() > currentRequest.size()){
            return currentRequest.subList(0,currentRequest.size());
        }else{
            return currentRequest.subList(0, useageBus.getSeat());
        }
    }

    private Queue deleteQueue(Queue reservedQueue) {
       try{
           queueRepository.delete(reservedQueue);
       }catch(Exception e){
           System.out.println(e);
       }
        return reservedQueue;
    }

    public List<Queue> rejectRequest(long timetableId) {
        List<Queue> queueByTimetableIdAndQueueType = getQueueByTimetableIdAndQueueType(timetableId, QueueType.requested);
        for (Queue queue : queueByTimetableIdAndQueueType) {
            queue.setQueueType(QueueType.unapprove);
            Queue reservedQueue = queue.getReservedQueue();
            queue.getReservedQueue().setQueueType(QueueType.booked);
            deleteQueue(reservedQueue);
        }
        return queueByTimetableIdAndQueueType;
    }

    public List<Queue> getHistoryByPassengerId(long passengerId) {
        return queueRepository.findByPassengerIdAndQueueType(passengerId,QueueType.departured);
    }

    public List<Queue> checkHourlyQueue() {
        LocalDateTime now = LocalDateTime.now();
        DayOfWeek dayOfWeek = now.getDayOfWeek();
        LocalTime localTime = LocalTime.now();
        LocalTime targetTime = LocalTime.of((localTime.getHour() + 1)%24, 0);
        List<Queue> activeRequest = queueRepository.findByQueueTypeAndTimetableDay(QueueType.requested,convertWeekDayToTimetableDay(dayOfWeek));
        List<Queue> targetQueue = new ArrayList<>();
        for (Queue q : activeRequest) {
            LocalTime queueTime = q.getTimetable().getTime();
            if (queueTime.compareTo(targetTime) < 1) {
                targetQueue.add(q);
            }
        }
        return unapprovedQueue(targetQueue);
    }

    private List<Queue> unapprovedQueue(List<Queue> targetQueue) {
        for (Queue q : targetQueue) {
            Queue reservedQueue = q.getReservedQueue();
            reservedQueue.setQueueType(QueueType.booked);
            q.setQueueType(QueueType.unapprove);
            q.setReservedQueue(reservedQueue);
            queueRepository.save(q);
        }
        return targetQueue;
    }

    public List<Queue> clearDailyQueue() {
        LocalDateTime now = LocalDateTime.now();
        DayOfWeek dayOfWeek = now.getDayOfWeek();
        List<Queue> activeRequest = queueRepository.findByQueueTypeAndTimetableDay(QueueType.requested,convertWeekDayToTimetableDay(dayOfWeek));
        unapprovedQueue(activeRequest);
        List<Queue> unProcessedQueue = queueRepository.findByQueueTypeAndTimetableDay(QueueType.booked,convertWeekDayToTimetableDay(dayOfWeek));
        for (Queue unQueue : unProcessedQueue) {
            unQueue.setQueueType(QueueType.missing);
        }
        return saveAllQueue(unProcessedQueue);
    }

    public long deleteQueueById(long targetQueueId) {
        Queue targetQueue = getQueueById(targetQueueId);
        List<Queue> userCanceledQueue = queueRepository.findByPassengerIdAndQueueTypeOrderByIdDesc(targetQueue.getPassenger().getId(), QueueType.canceled);
        if (userCanceledQueue.size() > 3) {
            userService.banUser(targetQueue.getPassenger().getUser());
        }
        return targetQueueId;
    }

}
