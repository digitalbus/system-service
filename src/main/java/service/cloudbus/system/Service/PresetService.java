package service.cloudbus.system.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.cloudbus.system.Model.PresetTimetable;
import service.cloudbus.system.Model.Timetable;
import service.cloudbus.system.Repository.PresetRepository;
import java.util.List;
import java.util.Set;

@Service
public class PresetService {

    @Autowired
    private PresetRepository presetRepo;

    @Autowired
    private TimetableService timetableService;

    public List<PresetTimetable> getAllPresetTimetable(){
        return presetRepo.findAll();
    }

    public PresetTimetable getPresetById(long presetId){ return presetRepo.getOne(presetId);}

    public PresetTimetable savePreset(PresetTimetable inputPreset) {
        return presetRepo.save(inputPreset);
    }

    public PresetTimetable editPreset (long presetId, PresetTimetable inputPreset){
        inputPreset.setId(presetId);
        return  presetRepo.save(inputPreset);
    }

    public  PresetTimetable deletePreset(long presetId){
        PresetTimetable targetPreset = presetRepo.findById(presetId);
        presetRepo.delete(targetPreset);
        return targetPreset;
    }

    public List<Timetable> activePresetById(long presetId) {
        timetableService.clearActiveTimetable();
        PresetTimetable targetPreset = presetRepo.getOne(presetId);
        Set<Timetable> targetTimetables = targetPreset.getPresetTimetables();
        for (Timetable timetable : targetTimetables) {
            timetable.setActive(true);
        }
        return timetableService.saveAll(targetTimetables);
    }
}
