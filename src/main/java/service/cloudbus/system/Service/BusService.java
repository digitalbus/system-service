package service.cloudbus.system.Service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import service.cloudbus.system.Model.Bus;
import service.cloudbus.system.Repository.BusRepository;

@Service
public class BusService {

    @Autowired
    private BusRepository busRepo;

    public List<Bus> getAllBus(){
        return busRepo.findAll();
    }

    public Bus getBusById(long busId){
        return busRepo.getOne(busId);
    }
    
    public Bus saveBus(Bus inputBus){
        return busRepo.save(inputBus);
    }

    public Bus editBus(long busId, Bus inputBus){
        inputBus.setId(busId);
        return busRepo.save(inputBus);
    }

    public Bus deleteBus(long busId){
        Bus targetBus = busRepo.getOne(busId);
        busRepo.delete(targetBus);
        return targetBus;
    }

    public Bus getBusByLicencePlate(String licence){
        return busRepo.findByLicencePlate(licence);
    }
}