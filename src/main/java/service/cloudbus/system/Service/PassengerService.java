package service.cloudbus.system.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.cloudbus.system.Model.Passenger;
import service.cloudbus.system.Model.User;
import service.cloudbus.system.Repository.PassengerRepository;

@Service
public class PassengerService {

    @Autowired
    private PassengerRepository passengerRepo;

    public Passenger getPassenger(User targetUser) {
        return passengerRepo.findByUser(targetUser);
    }

    public Passenger register(Passenger passenger) {
        passenger.setNewUser(true);
        return passengerRepo.save(passenger);
    }

    public Passenger findById(long targetPassenger) {
        return passengerRepo.findById(targetPassenger).get();
    }

    public Passenger getPassengerInfo(long passengerId) {
        return passengerRepo.findById(passengerId).get();
    }
}
