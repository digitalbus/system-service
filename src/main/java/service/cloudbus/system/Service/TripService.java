package service.cloudbus.system.Service;

import java.util.List;

import com.sun.org.apache.xalan.internal.xsltc.StripFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import service.cloudbus.system.Model.Queue;
import service.cloudbus.system.Model.QueueType;
import service.cloudbus.system.Model.Trip;
import service.cloudbus.system.Repository.TripRepository;

@Service
public class TripService {

    @Autowired
    private TripRepository tripRepo;

    @Autowired
    private QueueService queueService;

    public List<Trip> getAll(){
        return tripRepo.findAll();
    }

    public Trip getById(long tripId){
        return tripRepo.getOne(tripId);
    }

    public Trip save(Trip trip){
        return tripRepo.save(trip);
    }

    public Trip stampBus(Trip trip) {
        if (trip.getId() == 0) {
            trip = summariseQueueWithOutTrip(trip);
        }else{
            summariseQueueWithTrip(trip);
        }
        return save(trip);

    }

    private Trip summariseQueueWithTrip(Trip trip) {
        Trip targetTrip = getById(trip.getId());
        List<Queue> tripQueue = targetTrip.getQueues();
        for (Queue queue : tripQueue) {
            if (queue.getQueueType() == QueueType.checkedin) {
                queue.setQueueType(QueueType.departured);
            }else{
                queue.setQueueType(QueueType.missing);

            }
        }
        queueService.saveAllQueue(tripQueue);
        trip.setQueues(tripQueue);
        return trip;
    }


    public Trip summariseQueueWithOutTrip(Trip trip) {
        List<Queue> checkInQueue = queueService.getQueueByTimetableIdAndQueueType(trip.getTimetable().getId(), QueueType.checkedin);
        List<Queue> bookedQueue = queueService.getQueueByTimetableIdAndQueueType(trip.getTimetable().getId(), QueueType.booked);
        for (Queue queue : bookedQueue) {
            queue.setQueueType(QueueType.missing);
        }
        for (Queue cQueue : checkInQueue) {
            cQueue.setQueueType(QueueType.departured);
        }
        queueService.saveAllQueue(bookedQueue);
        trip.setQueues(checkInQueue);
        return trip;
    }

	public Trip getByBusDriverId(long id) {
		return null;
	}

    public Trip getByTimetableId(long timetableId) {
        return tripRepo.findByTimetableId(timetableId);
    }

    public List<Trip> findByTimetableIdAndBusStampIsNull(long timetableId) {
        return tripRepo.findByTimetableIdAndBusStampIsNull(timetableId);
    }

    public Trip stampBusByTrip(Trip trip) {
        return null;
    }


}