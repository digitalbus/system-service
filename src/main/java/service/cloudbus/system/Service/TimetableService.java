package service.cloudbus.system.Service;

import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import service.cloudbus.system.Model.*;
import service.cloudbus.system.Repository.TimetableRepository;

@Service
public class TimetableService {

    @Autowired
    private TimetableRepository timetableRepo;

    @Autowired
    private QueueService queueService;

    public Timetable getTimetableById(long timetableId) {
        return timetableRepo.getOne(timetableId);
    }

    public List<Timetable> getTimetableListByRouteIdAndDay(long routeId, Day selectedDay) {
        return timetableRepo.findByRouteIdAndDay(routeId, selectedDay);
    }

    public List<Timetable> getTimetableListByRouteIdAndDayAndIsActive(long routeId, Day selectedDay) {
        return timetableRepo.findByRouteIdAndDayAndIsActiveTrue(routeId, selectedDay);
    }

    public List<Timetable> getTimeSortedTimetableByRouteIdAndDay(long routeId, Day seletedDay) {
        List<Timetable> timetables = timetableRepo.findByRouteIdAndDayAndIsActiveTrue(routeId, seletedDay);
        LocalTime currentTime = LocalTime.now();
        Date now = new Date();
        SimpleDateFormat date = new SimpleDateFormat("EEEE");
        String today = date.format(now);
        List<Timetable> filtered = new ArrayList<>();
        List<Timetable> timetable = filterTimetable(seletedDay, currentTime, timetables, today);
        if (timetable != null) return timetable;
        return filtered;
    }

    private List<Timetable> filterTimetable(Day selected, LocalTime currentTime, List<Timetable> timetables, String today) {
        List<Timetable> filtered = new ArrayList<>();
        if (today.equalsIgnoreCase(selected.toString())) {
            for (Timetable timetable : timetables) {
                LocalTime departureTime = timetable.getTime();
                if (currentTime.compareTo(departureTime) < 0) {
                    filtered.add(calculateTimetables(timetable));
                }
            }
        } else {
            for (Timetable timetable : timetables) {
                filtered.add(calculateTimetables(timetable));
            }
        }
        return filtered;
    }

    public Timetable calculateTimetables(Timetable timetable) {
        long currentQueue = queueService.countActiveQueueByTimetable(timetable.getId());
        timetable.setCurrentActiveQueue(currentQueue);
        int maxSeat;
        if (timetable.getBusType() == BusType.bus) {
            maxSeat = 30;
        } else {
            maxSeat = 15;
        }
        timetable.setAvailableSeat((maxSeat - currentQueue));
        return timetable;
    }

    public Timetable saveTimetable(Timetable inputTimetable) {
        return timetableRepo.save(inputTimetable);
    }

    public Timetable deleteTimetable(long timetableId) {
        Timetable targetTimetable = timetableRepo.getOne(timetableId);
        timetableRepo.delete(targetTimetable);
        return targetTimetable;
    }

    public List<Timetable> getTimeSortedTimetableByDeparturePoint(RouteList departureAt, Day selectedDay) {
        List<Timetable> timetables = timetableRepo.findByRouteDepartureAndDayAndIsActiveTrue(departureAt, selectedDay);
        LocalTime currentTime = LocalTime.now();
        Date now = new Date();
        SimpleDateFormat date = new SimpleDateFormat("EEEE");
        String today = date.format(now);
        List<Timetable> filtered = filterTimetable(selectedDay, currentTime, timetables, today);
        return filtered;
    }

    public List<Timetable> setIsActiveToAll() {
        List<Timetable> all = timetableRepo.findAll();
        for (Timetable t : all) {
            t.setActive(true);
        }
        return timetableRepo.saveAll(all);
    }

    public List<Timetable> getAllTimetable(){
        return timetableRepo.findAll();
    }

    public void clearActiveTimetable() {
        List<Timetable> all = timetableRepo.findAll();
        for (Timetable timetable : all) {
            timetable.setActive(false);
        }
        timetableRepo.saveAll(all);
    }

    public List<Timetable> saveAll(Set<Timetable> targetTimetables) {
        return timetableRepo.saveAll(targetTimetables);
    }
}