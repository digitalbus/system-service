package service.cloudbus.system;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
public class SystemApplication  implements ApplicationRunner {

	private static final Logger logger = LogManager.getLogger(SystemApplication.class);


	@PostConstruct
	public void init(){
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Bangkok"));
	}

	public static void main(String[] args) {
		SpringApplication.run(SystemApplication.class, args);
	}


	@Override
	public void run(ApplicationArguments args) throws Exception {
		logger.info("Test Log");
		logger.debug("Test Debugging log");
		logger.warn("Hey, This is a warning!");
		logger.error("Oops! We have an Error. Just Kidding ");
		logger.fatal("Damn! Fatal error. Please Love me : )");
		logger.info("Every Thing is fine");
	}
}
