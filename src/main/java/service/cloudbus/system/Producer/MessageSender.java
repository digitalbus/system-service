package service.cloudbus.system.Producer;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.text.SimpleDateFormat;

public class MessageSender {

    @Autowired
    private SimpMessagingTemplate template;

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");


    public void sendMessage(long timetableId, int emptySeat) {
        template.convertAndSend("/queue/emptyseat/"+timetableId, emptySeat);
    }


    public void sendFetchMessage(long targetRouteId, boolean status) {
        template.convertAndSend("/queue/emptyseat/"+targetRouteId, status);
    }

    public void sendCheckedInMessage(long queueId) {
        template.convertAndSend("/queue/ticket/"+queueId,true);

    }
}
