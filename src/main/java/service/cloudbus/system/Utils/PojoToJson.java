package service.cloudbus.system.Utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import service.cloudbus.system.Model.Passenger;
import service.cloudbus.system.Model.Queue;
import service.cloudbus.system.Model.Route;
import service.cloudbus.system.Model.Timetable;

import java.util.HashMap;
import java.util.Map;

public class PojoToJson {

    @Autowired
    private static ObjectMapper objectMapper;

    public static Map<String, Object> queueToKibanaJson(Queue queue, Passenger passenger, Timetable timetable, Route route) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("id", queue.getId());
        data.put("queueType", queue.getQueueType());
        data.put("faculty", passenger.getFacuty());
        data.put("major", passenger.getMajor());
        data.put("time", timetable.getTime());
        data.put("day", timetable.getDay());
        data.put("departure", route.getDeparture());
        data.put("arrival", route.getArrival());
        data.put("day", timetable.getDay());
        data.put("lat", queue.getLatitude());
        data.put("lon", queue.getLongitude());
        data.put("location", queue.getLatitude()+","+queue.getLongitude());
        data.put("vehicleType", timetable.getBusType());
        if (queue.getMockDate() != null) {
            data.put("createAt", queue.getMockDate());
        }else {
            data.put("createAt", queue.getCreateAt());
        }
        return data;
    }

    public static Map<String, Object> queueToJson(Queue queue) {
        return objectMapper.convertValue(queue, Map.class);
    }

}
