package service.cloudbus.system.Utils;

import service.cloudbus.system.Model.Day;

import java.time.DayOfWeek;

public class Timeutils {
    public static Day convertWeekDayToTimetableDay(DayOfWeek dayOfWeek) {
        int day = dayOfWeek.getValue();
        switch (day) {
            case 1:
                return Day.monday;
            case 2:
                return Day.tuesday;
            case 3:
                return Day.wednesday;
            case 4:
                return Day.thursday;
            case 5:
                return Day.friday;
            case 6:
                return Day.saturday;
            case 7:
                return Day.sunday;
            default:
                return Day.valueOf(day+"");
        }
    }
}
