package service.cloudbus.system.Configuration;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import service.cloudbus.system.Producer.MessageSender;

@Configuration
public class MessageConfiguration {

    @Bean
    public TopicExchange topic() {
        return new TopicExchange("messageTopic.topic");
    }

    @Bean
    public MessageSender sender() {
        return new MessageSender();
    }
}
