package service.cloudbus.system.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "queue")
public class Queue {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    @Enumerated(EnumType.STRING)
    private QueueType queueType;

    @ManyToOne(fetch = FetchType.EAGER)
    private Passenger passenger;

    @ManyToOne(fetch = FetchType.EAGER)
    private Timetable timetable;

    @ManyToOne
    @JsonIgnore
    private Trip trip;

    @CreationTimestamp
    private LocalDateTime createAt;

    @UpdateTimestamp
    private LocalDateTime updateAt;

    @OneToOne(cascade = CascadeType.ALL)
    private Queue reservedQueue;


    @Transient
    private int queueNumber;

    @Transient
    private float latitude;

    @Transient
    private float longitude;

    @Transient
    private Date mockDate;


    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public QueueType getQueueType() {
        return queueType;
    }

    public void setQueueType(QueueType queueType) {
        this.queueType = queueType;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public Timetable getTimetable() {
        return timetable;
    }

    public void setTimetable(Timetable timetable) {
        this.timetable = timetable;
    }

    public LocalDateTime getCreateAt() {
        return createAt;
    }

    public void setCreateAt(LocalDateTime createAt) {
        this.createAt = createAt;
    }

    public LocalDateTime getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(LocalDateTime updateAt) {
        this.updateAt = updateAt;
    }

    public int getQueueNumber() {
        return queueNumber;
    }

    public void setQueueNumber(int queueNumber) {
        this.queueNumber = queueNumber;
    }

    public Queue getReservedQueue() {
        return reservedQueue;
    }

    public void setReservedQueue(Queue reservedQueue) {
        this.reservedQueue = reservedQueue;
    }

    public Date getMockDate() {
        return mockDate;
    }

    public void setMockDate(Date mockDate) {
        this.mockDate = mockDate;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    @Override
    public String toString() {
        return "Queue{" +
                "id=" + id +
                ", queueType=" + queueType +
                ", passenger=" + passenger +
                ", timetable=" + timetable +
                ", trip=" + trip +
                ", createAt=" + createAt +
                ", updateAt=" + updateAt +
                ", reservedQueue=" + reservedQueue +
                ", queueNumber=" + queueNumber +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", mockDate=" + mockDate +
                '}';
    }

}
