package service.cloudbus.system.Model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "bus")
public class Bus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String licencePlate;

    @NotNull
    private int seat; 

    @NotNull
    @Enumerated(EnumType.STRING)
    private BusType busType;

    public Bus(long id, String licencePlate, int seat, BusType busType) {
        this.id = id;
        this.licencePlate = licencePlate;
        this.seat = seat;
        this.busType = busType;
    }

    public BusType getBusType() {
        return this.busType;
    }

    public void setBusType(BusType busType) {
        this.busType = busType;
    }

    public Bus busType(BusType busType) {
        this.busType = busType;
        return this;
    }

    public Bus() {
    }

    public Bus(long id, String licencePlate, int seat) {
        this.id = id;
        this.licencePlate = licencePlate;
        this.seat = seat;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getlicencePlate() {
        return this.licencePlate;
    }

    public void setlicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public int getSeat() {
        return this.seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public Bus id(long id) {
        this.id = id;
        return this;
    }

    public Bus licencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
        return this;
    }

    public Bus seat(int seat) {
        this.seat = seat;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", licencePlate='" + getlicencePlate() + "'" +
            ", seat='" + getSeat() + "'" +
            "}";
    }

}