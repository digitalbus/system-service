package service.cloudbus.system.Model;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "passenger")
public class Passenger {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "userId")
    private User user;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @Column(unique = true)
    private String identification;

    private String facuty;

    private String major;

    private String profileUrl;

    @Transient
    private boolean newUser = false;

    public Passenger() {
    }

    public Passenger(long id, User user, String firstName, String lastName, String identification, String facuty, String major) {
        this.id = id;
        this.user = user;
        this.firstName = firstName;
        this.lastName = lastName;
        this.identification = identification;
        this.facuty = facuty;
        this.major = major;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdentification() {
        return this.identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getFacuty() {
        return this.facuty;
    }

    public void setFacuty(String facuty) {
        this.facuty = facuty;
    }

    public String getMajor() {
        return this.major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public Passenger id(long id) {
        this.id = id;
        return this;
    }


    public Passenger firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public Passenger lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public Passenger identification(String identification) {
        this.identification = identification;
        return this;
    }

    public Passenger facuty(String facuty) {
        this.facuty = facuty;
        return this;
    }

    public Passenger major(String major) {
        this.major = major;
        return this;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public boolean isNewUser() {
        return newUser;
    }

    public void setNewUser(boolean newUser) {
        this.newUser = newUser;
    }

    @Override
    public String toString() {
        return "Passenger{" +
                "id=" + id +
                ", user=" + user +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", identification='" + identification + '\'' +
                ", facuty='" + facuty + '\'' +
                ", major='" + major + '\'' +
                ", profileUrl='" + profileUrl + '\'' +
                ", newUser=" + newUser +
                '}';
    }
}