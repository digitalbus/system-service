package service.cloudbus.system.Model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "bus_driver")
public class BusDriver {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String kmuttId;

    @NotNull
    private String firstName;

    @NotNull
    private String lastname;

    public BusDriver() {
    }

    public BusDriver(long id, String kmuttId, String firstName, String lastname) {
        this.id = id;
        this.kmuttId = kmuttId;
        this.firstName = firstName;
        this.lastname = lastname;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getKmuttId() {
        return this.kmuttId;
    }

    public void setKmuttId(String kmuttId) {
        this.kmuttId = kmuttId;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public BusDriver id(long id) {
        this.id = id;
        return this;
    }

    public BusDriver kmuttId(String kmuttId) {
        this.kmuttId = kmuttId;
        return this;
    }

    public BusDriver firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public BusDriver lastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", kmuttId='" + getKmuttId() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", lastname='" + getLastname() + "'" +
            "}";
    }
}