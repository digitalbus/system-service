package service.cloudbus.system.Model;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "trip")
public class Trip {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "timetableId")
    private Timetable timetable;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "busId")
    private Bus bus;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "busDriverId")
    private BusDriver driver;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "trip_id")
    private List<Queue> queues;

    @Column(name = "busStamp")
    private Date busStamp;

    @Column
	@CreationTimestamp
	private LocalDateTime createDateTime;

	@Column
	@UpdateTimestamp
	private LocalDateTime updateDateTime;   

    public Trip() {
    }

    public Trip(Timetable timetable, Bus bus, BusDriver driver, Date busStamp) {
        this.timetable = timetable;
        this.bus = bus;
        this.driver = driver;
        this.busStamp = busStamp;
    }
    public Trip(long id, Timetable timetable, Bus bus, BusDriver driver, List<Queue> queues, Date busStamp, LocalDateTime createDateTime, LocalDateTime updateDateTime) {
        this.id = id;
        this.timetable = timetable;
        this.bus = bus;
        this.driver = driver;
        this.queues = queues;
        this.busStamp = busStamp;
        this.createDateTime = createDateTime;
        this.updateDateTime = updateDateTime;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timetable getTimetable() {
        return this.timetable;
    }

    public void setTimetable(Timetable timetable) {
        this.timetable = timetable;
    }

    public Bus getBus() {
        return this.bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public BusDriver getDriver() {
        return this.driver;
    }

    public void setDriver(BusDriver driver) {
        this.driver = driver;
    }

    public Date getBusStamp() {
        return this.busStamp;
    }

    public void setBusStamp(Date busStamp) {
        this.busStamp = busStamp;
    }

    public LocalDateTime getCreateDateTime() {
        return this.createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public LocalDateTime getUpdateDateTime() {
        return this.updateDateTime;
    }

    public void setUpdateDateTime(LocalDateTime updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public Trip createDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
        return this;
    }

    public Trip updateDateTime(LocalDateTime updateDateTime) {
        this.updateDateTime = updateDateTime;
        return this;
    }

    public List<Queue> getQueues() {
        return queues;
    }

    public void setQueues(List<Queue> queues) {
        this.queues = queues;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "id=" + id +
                ", timetable=" + timetable +
                ", bus=" + bus +
                ", driver=" + driver +
                ", queues=" + queues +
                ", busStamp=" + busStamp +
                ", createDateTime=" + createDateTime +
                ", updateDateTime=" + updateDateTime +
                '}';
    }
}