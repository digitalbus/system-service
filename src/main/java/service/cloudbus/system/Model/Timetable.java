package service.cloudbus.system.Model;

import java.time.LocalTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "timetable")
public class Timetable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.DETACH, optional = false)
    @JoinColumn(name = "routeId")
    private Route route;


    private LocalTime time;

    @NotNull
    private int standbyBus;

    @NotNull
    @Enumerated(EnumType.STRING)
    private BusType busType;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Day day;

    @Transient
    private long availableSeat;

    @Transient
    private long currentActiveQueue;

    @Transient
    private long currentRequestQueue;

    private boolean isActive;

    public Timetable() {
    }

    public Timetable(long id, Route route, LocalTime time, int standbyBus, BusType busType, Day day) {
        this.id = id;
        this.route = route;
        this.time = time;
        this.standbyBus = standbyBus;
        this.busType = busType;
        this.day = day;
    }

    public Timetable(Route route, LocalTime time, @NotNull int standbyBus, @NotNull BusType busType, @NotNull Day day, int availableSeat) {
        this.route = route;
        this.time = time;
        this.standbyBus = standbyBus;
        this.busType = busType;
        this.day = day;
        this.availableSeat = availableSeat;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Route getRoute() {
        return this.route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public LocalTime getTime() {
        return this.time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public int getStandbyBus() {
        return this.standbyBus;
    }

    public void setStandbyBus(int standbyBus) {
        this.standbyBus = standbyBus;
    }

    public BusType getBusType() {
        return this.busType;
    }

    public void setBusType(BusType busType) {
        this.busType = busType;
    }

    public Day getDay() {
        return this.day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Timetable id(long id) {
        this.id = id;
        return this;
    }

    public Timetable route(Route route) {
        this.route = route;
        return this;
    }

    public Timetable time(LocalTime time) {
        this.time = time;
        return this;
    }

    public Timetable standbyBus(int standbyBus) {
        this.standbyBus = standbyBus;
        return this;
    }

    public Timetable busType(BusType busType) {
        this.busType = busType;
        return this;
    }

    public Timetable day(Day day) {
        this.day = day;
        return this;
    }

    public long getAvailableSeat() {
        return availableSeat;
    }

    public void setAvailableSeat(long availableSeat) {
        this.availableSeat = availableSeat;
    }

    public long getCurrentActiveQueue() {
        return currentActiveQueue;
    }

    public void setCurrentActiveQueue(long currentActiveQueue) {
        this.currentActiveQueue = currentActiveQueue;
    }

    public long getCurrentRequestQueue() {
        return currentRequestQueue;
    }

    public void setCurrentRequestQueue(long currentRequestQueue) {
        this.currentRequestQueue = currentRequestQueue;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "Timetable{" +
                "id=" + id +
                ", route=" + route +
                ", time=" + time +
                ", standbyBus=" + standbyBus +
                ", busType=" + busType +
                ", day=" + day +
                ", availableSeat=" + availableSeat +
                ", currentActiveQueue=" + currentActiveQueue +
                ", currentRequestQueue=" + currentRequestQueue +
                ", isActive=" + isActive +
                '}';
    }

}
