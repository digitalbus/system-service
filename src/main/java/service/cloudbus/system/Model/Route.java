package service.cloudbus.system.Model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import ch.qos.logback.core.subst.Token.Type;


@Entity
@Table(name = "route")
public class Route {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(EnumType.STRING)
    private RouteList departure;

    @Enumerated(EnumType.STRING)
    private RouteList arrival;

    public Route() {
    }

    public Route(long id, RouteList departure, RouteList arrival) {
        this.id = id;
        this.departure = departure;
        this.arrival = arrival;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public RouteList getDeparture() {
        return this.departure;
    }

    public void setDeparture(RouteList departure) {
        this.departure = departure;
    }

    public RouteList getArrival() {
        return this.arrival;
    }

    public void setArrival(RouteList arrival) {
        this.arrival = arrival;
    }

    public Route id(long id) {
        this.id = id;
        return this;
    }

    public Route departure(RouteList departure) {
        this.departure = departure;
        return this;
    }

    public Route arrival(RouteList arrival) {
        this.arrival = arrival;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Route)) {
            return false;
        }
        Route route = (Route) o;
        return id == route.id && Objects.equals(departure, route.departure) && Objects.equals(arrival, route.arrival);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, departure, arrival);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", departure='" + getDeparture() + "'" +
            ", arrival='" + getArrival() + "'" +
            "}";
    }

    
}