package service.cloudbus.system.Model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name="Preset")
public class PresetTimetable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String namePreset;

    @NotNull
    private String description;

    @ManyToMany(cascade = CascadeType.DETACH)
    @JoinTable(name= "preset_timetable",
            joinColumns = { @JoinColumn (name="preset_id")},
            inverseJoinColumns = { @JoinColumn(name="timetable_id")}
    )
    Set<Timetable> presetTimetables;

    private boolean isActive;

    public PresetTimetable() {
    }

    public PresetTimetable(@NotNull String namePreset, @NotNull String description, Set<Timetable> presetTimetables) {
        this.namePreset = namePreset;
        this.description = description;
        this.presetTimetables = presetTimetables;
    }

    public PresetTimetable(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNamePreset() {
        return namePreset;
    }

    public void setNamePreset(String namePreset) {
        this.namePreset = namePreset;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Timetable> getPresetTimetables() {
        return presetTimetables;
    }

    public void setPresetTimetables(Set<Timetable> presetTimetables) {
        this.presetTimetables = presetTimetables;
    }

    @Override
    public String toString() {
        return "PresetTimetable{" +
                "id=" + id +
                ", namePreset='" + namePreset + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
