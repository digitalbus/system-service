package service.cloudbus.system.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class QueuingException extends RuntimeException {

    public QueuingException() {
    }

    public QueuingException(String message) {
        super(message);
    }

    public QueuingException(Throwable thrwbl) {
        super(thrwbl);
    }

    public QueuingException(String message, Throwable thrwbl) {
        super(message, thrwbl);
    }
}
