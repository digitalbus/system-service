package service.cloudbus.system.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class ConflictException extends RuntimeException {
    public ConflictException() {
    }

    public ConflictException(String message) {
        super(message);
    }

    public ConflictException(Throwable thrwbl) {
        super(thrwbl);
    }

    public ConflictException(String message, Throwable thrwbl) {
        super(message, thrwbl);
    }
}
