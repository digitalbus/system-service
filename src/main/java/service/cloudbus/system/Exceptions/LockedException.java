package service.cloudbus.system.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.LOCKED)
public class LockedException extends RuntimeException {
    public LockedException() {
    }

    public LockedException(String message) {
        super(message);
    }

    public LockedException(Throwable thrwbl) {
        super(thrwbl);
    }

    public LockedException(String message, Throwable thrwbl) {
        super(message, thrwbl);
    }
}
