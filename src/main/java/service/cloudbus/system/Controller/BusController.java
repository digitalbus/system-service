package service.cloudbus.system.Controller;

import org.springframework.web.bind.annotation.*;

import service.cloudbus.system.Service.BusService;

import service.cloudbus.system.Model.Bus;

import java.util.List;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;


@RestController
@CrossOrigin("*")
public class BusController {

    @Autowired
    private BusService busService;

    @GetMapping("system/bus")
    public List<Bus> getAllBus() {
        return busService.getAllBus();
    }

    @GetMapping("system/bus/{busId}")
    public Bus getBusByBusId(@PathVariable long busId) {
        return busService.getBusById(busId);
    }

    @PostMapping("system/bus")
    public Bus saveBus(@RequestBody Bus inputBus) {
        return busService.saveBus(inputBus);
    }

    @PutMapping("/system/bus/{id}")
    public Bus updateBus(@PathVariable(value = "id") Long busId, @Valid @RequestBody Bus busDetails) {
        Bus bus = busService.editBus(busId, busDetails);
        return bus;
    }

    @GetMapping("system/bus/plate")
    public Bus getBusByLicencePlate(@PathParam("licencePlate") String licencePlate) {
        System.out.println(licencePlate);
        return busService.getBusByLicencePlate(licencePlate);
    }

    @DeleteMapping("/system/bus/{busId}")
    public Bus deleteBus(@PathVariable long busId) {
        return busService.deleteBus(busId);
    }
    
}