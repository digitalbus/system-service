package service.cloudbus.system.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import service.cloudbus.system.Model.BusDriver;
import service.cloudbus.system.Service.BusDriverService;

import java.util.List;

@RestController
@CrossOrigin("*")
public class BusDriverController{
    
    @Autowired
    private BusDriverService busDriverService;

    @PostMapping("system/bus_driver")
    public BusDriver createBusDriver(@RequestBody BusDriver busDriver) {
        return busDriverService.createBusDriver(busDriver);
    }

    @GetMapping("system/bus_driver")
    public List<BusDriver> getAllDriver() {
        return busDriverService.getAllDriver();
    }

    @GetMapping("system/bus_driver/{driverId}")
    public BusDriver getBusDriverById(@PathVariable long driverId) {
        return busDriverService.getBusDriverById(driverId);
    }

    @DeleteMapping("/system/bus_driver/{driverId}")
    public BusDriver deleteDriver(@PathVariable long driverId) {
        return busDriverService.deleteDriver(driverId);
    }
}