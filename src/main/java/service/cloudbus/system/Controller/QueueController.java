package service.cloudbus.system.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.cloudbus.system.Model.*;
import service.cloudbus.system.Service.QueueService;

import java.text.SimpleDateFormat;
import java.util.List;

@RestController
@CrossOrigin("*")
public class QueueController {

    @Autowired
    private QueueService queueService;

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");


    @GetMapping("queue")
    public String helthCheck() {
        return "OK!";
    }

    @PostMapping("queue/request")
    public Queue requestQueue(@RequestBody Queue requestQueue) {
        return queueService.requestQueue(requestQueue);
    }


    @PostMapping("queue")
    public Queue createQueue(@RequestBody Queue inputQueue) {
        return queueService.bookQueue(inputQueue);
    }

    @GetMapping("queue/{queueId}")
    public Queue getQueueById(@PathVariable long queueId) {
        return queueService.getQueueById(queueId);
    }

    @GetMapping("queue/checkin/{queueId}")
    public Queue checkinQueueById(@PathVariable long queueId) {
        return queueService.checkinQueueById(queueId);
    }

    @GetMapping("queue/timetable/status/{timetableId}")
    public List<Queue> timetableQueueStatus(@PathVariable long timetableId) {
        return queueService.getCheckinStatusByTimetableId(timetableId);
    }

    @GetMapping("queue/trip/status/{tripId}")
    public List<Queue> tripQueueStatus(@PathVariable long tripId) {
        return queueService.getCheckinStatusByTripId(tripId);
    }

    @GetMapping("queue/count/timetable/{timetableId}")
    public long countTimetableByTimetableId(@PathVariable long timetableId) {
        return queueService.countTimetableByTimetableId(timetableId);
    }

    @GetMapping("queue/count/active/timetable/{timetableId}")
    public long countActiveQueueByTimetableId(@PathVariable long timetableId) {
        return queueService.countActiveQueueByTimetable(timetableId);
    }

    @GetMapping("queue/myticket/{id}")
    public List<Queue> myTicket(@PathVariable long id) {
        return queueService.getMyTicket(id);
    }

    @GetMapping("queue/queueNumber/{queueId}")
    public int queueNumberById(@PathVariable long queueId) {
        return queueService.getQueueNumberById(queueId);
    }


    @GetMapping("queue/update/{timetableId}")
    public int emptySeat(@PathVariable long timetableId) {
        return queueService.sendMessage(timetableId);
    }

    @GetMapping("queue/request/number")
    public int getAllRequestNumber() {
        return queueService.getAllRequest().size();
    }

    @GetMapping("queue/request")
    public List<Timetable> getAllRequest() {
        return queueService.getAllRequest();
    }

    @PostMapping("queue/approve")
    public List<Queue> approveRequest(@RequestBody Trip trip) {
        return queueService.approveRequets(trip);
    }

    @DeleteMapping("queue/reject")
    public List<Queue> rejectRequest(@RequestParam("timetable_id") long timetableId) {
        return queueService.rejectRequest(timetableId);
    }

    @GetMapping("queue/history/{passengerId}")
    public List<Queue> queueHistory(@PathVariable long passengerId) {
        return queueService.getHistoryByPassengerId(passengerId);
    }

    @GetMapping("queue/hourly")
    public List<Queue> hourlyCheckQueue() {
        return queueService.checkHourlyQueue();
    }

    @DeleteMapping("queue/{targetQueueId}")
    public long deleteQueue(@PathVariable long targetQueueId){
        return queueService.deleteQueueById(targetQueueId);
    }
}
