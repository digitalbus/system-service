package service.cloudbus.system.Controller;

import org.springframework.web.bind.annotation.RestController;

import service.cloudbus.system.Model.Route;
import service.cloudbus.system.Model.RouteList;
import service.cloudbus.system.Service.RouteService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
@CrossOrigin("*")
public class RouteController {

    @Autowired
    private RouteService routeService;

    @GetMapping("system/route")
    public List<Route> getAllRoute() {
        return routeService.getAllRoute();
    }

    @GetMapping("system/route/{routeId}")
    public Route getRouteById(@PathVariable(value = "routeId")long routeId) {
        return routeService.getRouteById(routeId);}

    @GetMapping("system/route/departure")
    public List<Route> getRouteByDepartureId(@RequestParam(name = "departureAt") RouteList departureLocation) {
        return routeService.getRouteByDeparture(departureLocation);
    }

    @PostMapping("system/route")
    public Route saveRoute(@RequestBody Route inputRoute){
        return routeService.saveRoute(inputRoute);
    }
    
}