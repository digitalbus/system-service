package service.cloudbus.system.Controller;

import org.springframework.web.bind.annotation.*;
import service.cloudbus.system.Service.BusDriverService;
import service.cloudbus.system.Service.BusService;
import service.cloudbus.system.Service.TimetableService;
import service.cloudbus.system.Service.TripService;
import service.cloudbus.system.Model.Trip;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

@RestController
@CrossOrigin("*")
public class TripController {

    @Autowired
    private TripService tripService;

    @Autowired
    private BusService busService;

    @Autowired
    private TimetableService timetableService;

    @Autowired
    private BusDriverService busDriverService;

    @GetMapping("system/trip")
    public List<Trip> getAll() {
        return tripService.getAll();
    }

    @GetMapping("system/trip/{tripId}")
    public Trip getById(@PathVariable long tripId) {
        return tripService.getById(tripId);
    }


    @PostMapping("system/trip")
    public Trip saveTrip(@RequestBody Trip trip) {
        return tripService.save(trip);
    }

    @PostMapping("system/trip/stampBus")
    public Trip stampBus(@RequestBody Trip trip) {
        return tripService.stampBus(trip);
    }

    @GetMapping("system/trip/timetable/{timetableId}")
    public Trip getTripByTimetableId(@PathVariable long timetableId) {
        return tripService.getByTimetableId(timetableId);
    }

    @GetMapping("system/trip/busDriver/{id}")
    public Trip getTripByBusDriverId(@PathVariable long id) {
        return null;
    }

    @GetMapping("system/trip/ongoing/timetable/{timetableId}")
    public List<Trip> getTripFromTimetable(@PathVariable long timetableId) {
        return tripService.findByTimetableIdAndBusStampIsNull(timetableId);
    }


    @PostMapping("system/requesttrip/stampbus")
    public Trip busStampByTrip(@RequestBody Trip trip) {
        return tripService.stampBusByTrip(trip);
    }
}