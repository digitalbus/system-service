package service.cloudbus.system.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import service.cloudbus.system.Model.PresetTimetable;
import service.cloudbus.system.Model.Timetable;
import service.cloudbus.system.Service.PresetService;
import org.springframework.web.bind.annotation.*;
import service.cloudbus.system.Service.TimetableService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin("*")
public class PresetController {

    @Autowired
    private PresetService presetService;

    @Autowired
    private TimetableService timetableService;

    @GetMapping("system/preset")
    public List<PresetTimetable> getALlPreset(){
        return presetService.getAllPresetTimetable();
    }

    @GetMapping("system/preset/{presetId}")
    public PresetTimetable getPresetById(@PathVariable long presetId){
        return presetService.getPresetById(presetId);
    }

    @GetMapping("system/preset/active/{presetId}")
    public List<Timetable> activePresetById(@PathVariable long presetId) {
        return presetService.activePresetById(presetId);
    }

    @PostMapping("system/preset")
    public PresetTimetable savePreset(@RequestBody PresetTimetable inputPreset) {
        return presetService.savePreset(inputPreset);
    }

    @PutMapping("system/preset/{id}")
    public PresetTimetable editPreset(@PathVariable(value="id") long presetId, @Valid @RequestBody PresetTimetable presetDetails){
        PresetTimetable preset = presetService.editPreset(presetId, presetDetails);
        return preset;
    }

    @DeleteMapping("system/preset/{presetId}")
    public PresetTimetable deletePreset(@PathVariable long presetId){
        return presetService.deletePreset(presetId);
    }
}
