package service.cloudbus.system.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.cloudbus.system.Model.Passenger;
import service.cloudbus.system.Model.User;
import service.cloudbus.system.Service.PassengerService;
import service.cloudbus.system.Service.UserService;

@RestController
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private PassengerService passengerService;

    @PostMapping("/system/user/login")
    public Passenger login(@RequestBody User user) {
        return userService.loginUser(user);
    }

    @PostMapping("/system/facebook/login")
    public Passenger facebooklogIn(@RequestBody Passenger passenger) {
        return userService.facebookLogin(passenger);
    }

    @PostMapping("/system/user/register")
    public Passenger register(@RequestBody Passenger passenger) {
        return passengerService.register(passenger);
    }

    @GetMapping("/system/user/info/{passengerId}")
    public Passenger getPassengerInfo(@PathVariable long passengerId) {
        return passengerService.getPassengerInfo(passengerId);
    }
}
