package service.cloudbus.system.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.cloudbus.system.Model.Day;
import service.cloudbus.system.Model.RouteList;
import service.cloudbus.system.Model.Timetable;
import service.cloudbus.system.Service.TimetableService;

import java.util.List;

@RestController
@CrossOrigin("*")
public class TimetableController {

    @Autowired
    private TimetableService timetableService;

    @GetMapping("system/timetable/applyisactive")
    public List<Timetable> applyIsActive(){
        return timetableService.setIsActiveToAll();
    }

    @GetMapping("system/timetable/route/{routeId}")
    public List<Timetable> getTimetableListByRouteIdAndIsActive(@PathVariable int routeId, @RequestParam(name = "day") Day seletedDay) {
        return timetableService.getTimetableListByRouteIdAndDayAndIsActive(routeId, seletedDay);
    }

    @GetMapping("system/timetable/routebytime/{routeId}")
    public List<Timetable> getTimesortedTimetableByRouteId(@PathVariable int routeId, @RequestParam(name = "day") Day seletedDay) {
        return timetableService.getTimeSortedTimetableByRouteIdAndDay(routeId, seletedDay);
    }

    @GetMapping("system/timetable")
    public List<Timetable> getTimeSortedTimetableByDeparturePoint(@RequestParam(name = "dep") RouteList departureAt,@RequestParam(name = "day") Day selectedDay) {
        return timetableService.getTimeSortedTimetableByDeparturePoint(departureAt,selectedDay);
    }


    @GetMapping("/system/timetable/{timetableId}")
    public Timetable getTimetableByTimetableId(@PathVariable int timetableId) {
        return timetableService.getTimetableById(timetableId);
    }

    @PostMapping("/system/timetable")
    public Timetable saveTimetable(@RequestBody Timetable inputTimetable) {
        return timetableService.saveTimetable(inputTimetable);
    }

    @DeleteMapping("/system/timetable/{timetableId}")
    public Timetable deleteTimetable(@PathVariable int timetableId) {
        return timetableService.deleteTimetable(timetableId);
    }

    @GetMapping("system/timetable/all/{routeId}")
    public List<Timetable> getTimetableByRouteId(@PathVariable int routeId, @RequestParam(name = "day") Day seletedDay) {
        return timetableService.getTimetableListByRouteIdAndDay(routeId, seletedDay);
    }

    @GetMapping("system/timetable/all")
    public List<Timetable> getAllTimetable(){
        return timetableService.getAllTimetable();
    }
}