package service.cloudbus.system.Scheduled;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import service.cloudbus.system.Model.Queue;
import service.cloudbus.system.Service.QueueService;

import java.util.List;

@Component
public class MyScheduler{

    @Autowired
    QueueService queueService;

    @Scheduled(cron="0 0 */1 * * *")
    public void checkIsAnyRequestInTheNextHour() {
        List<Queue> queues = queueService.checkHourlyQueue();
        System.out.println("hourjob");
    }

    @Scheduled(cron="0 50 23 * * * ")
    public void clearQueueDaily() {
        List<Queue> queues = queueService.clearDailyQueue();
        System.out.println("dayjob");
    }
}
