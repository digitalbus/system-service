package service.cloudbus.system.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import service.cloudbus.system.Model.Route;
import service.cloudbus.system.Model.RouteList;

@Repository
public interface RouteRepository extends JpaRepository<Route, Long>{
   
    List<Route> findByDeparture(RouteList TargetDeparture);
    
}