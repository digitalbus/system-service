package service.cloudbus.system.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import service.cloudbus.system.Model.Day;
import service.cloudbus.system.Model.RouteList;
import service.cloudbus.system.Model.Timetable;

@Repository
public interface TimetableRepository extends JpaRepository<Timetable, Long>{
    
    List<Timetable> findByRouteId(long routeId);
    List<Timetable> findByRouteIdAndDayAndIsActiveTrue(long routeId, Day selectedDay);
    List<Timetable> findByRouteDepartureAndDayOrderByTime(RouteList departureAt, Day selectedDay);
    List<Timetable> findByRouteDepartureAndDayAndIsActiveTrue(RouteList departureAt, Day selectedDay);
    List<Timetable> findByRouteIdAndDay(long routeId, Day selectedDay);
}
  