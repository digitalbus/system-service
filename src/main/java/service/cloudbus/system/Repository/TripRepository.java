package service.cloudbus.system.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import service.cloudbus.system.Model.Trip;

import java.util.List;

@Repository
public interface TripRepository extends JpaRepository<Trip, Long>{
    Trip findByTimetableId(long timetableId);

    List<Trip> findByTimetableIdAndBusStampIsNull(long timetableId);

}