package service.cloudbus.system.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import service.cloudbus.system.Model.Day;
import service.cloudbus.system.Model.PresetTimetable;


public interface PresetRepository extends JpaRepository<PresetTimetable, Long> {

    PresetTimetable findById (long id);
}
