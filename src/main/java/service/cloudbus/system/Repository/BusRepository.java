package service.cloudbus.system.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import service.cloudbus.system.Model.Bus;

@Repository
public interface BusRepository extends JpaRepository<Bus, Long>{
    Bus findById(long id);

    Bus findByLicencePlate(String licencePlate);
}