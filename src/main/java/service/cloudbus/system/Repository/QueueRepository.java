package service.cloudbus.system.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import service.cloudbus.system.Model.Day;
import service.cloudbus.system.Model.Queue;
import service.cloudbus.system.Model.QueueType;

@Repository
 public interface QueueRepository extends JpaRepository<Queue, Long>{
     Queue findByIdAndQueueType(long id, QueueType queueType);

     List<Queue> findByTimetableIdAndQueueTypeAndTripIsNull(long id, QueueType queueType);

     long countByTimetableId(long timetableId);

     long countByTimetableIdAndQueueType(long timetableId,QueueType queueType);

    Queue findByPassengerId(long id);

    Queue findByPassengerIdAndTimetableIdAndQueueType(long passengerId, long timetableId, QueueType reserve);

    List<Queue> findByPassengerIdAndQueueTypeOrderByIdDesc(long targetUserId,QueueType queueType);

    List<Queue> findByPassengerIdAndQueueTypeOrQueueTypeOrderByIdDesc(long targetUserId,QueueType queueType,QueueType orQueueType);

    List<Queue> findByTimetableId(long targetTimetableId);

    List<Queue> findByQueueType(QueueType requested);

    List<Queue> findByTripId(long tripId);

    List<Queue> findByPassengerIdAndQueueType(long passengerId, QueueType queueType);

    List<Queue> findByQueueTypeAndTimetableDay(QueueType queueType, Day targetDay);
}