package service.cloudbus.system.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import service.cloudbus.system.Model.Timetable;
import service.cloudbus.system.Model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsernameAndPassword(String username, String password);

    User findByFacebookId(String facebookId);

}
