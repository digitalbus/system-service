package service.cloudbus.system.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import service.cloudbus.system.Model.Passenger;
import service.cloudbus.system.Model.User;

@Repository
public interface PassengerRepository extends JpaRepository<Passenger, Long> {
    Passenger findByUser(User targetUser);
}
