package service.cloudbus.system.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import service.cloudbus.system.Model.BusDriver;

@Repository
public interface BusDriverRepository extends JpaRepository<BusDriver, Long> {
    BusDriver findById(long driverId);
}
